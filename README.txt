Repository link: https://bitbucket.org/bobfish/fit2096-assignment-2b/src/master/

Bonus features:
	- Added a slow down time feature. When you press the shift key the game will slow down to
	  1/4 the usual speed. Will turn off when you press shift again or when the power runs out
	  (displayed at top of screen)
	  
	- Added Sounds and music:
		- Background sounds all made by me using the sfxr program
			- Shoot
			- Hit by bullet (player only)
			- Use powerup
		- Background music is royalty free track "Tactics" by Alex Lisi
		  (https://www.youtube.com/watch?v=GNzEtud5rFM)