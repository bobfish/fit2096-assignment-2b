#include "CollisionManager.h"

CollisionManager::CollisionManager(std::vector<Player*>* Players, std::vector<Monster*>* Monsters, std::vector<Bullet*>* Bullets)
{
	m_Players = Players;
	m_Monsters = Monsters;
	m_Bullets = Bullets;

	// Clear our arrays to 0 (NULL)
	memset(m_currentCollisions, 0, sizeof(m_currentCollisions));
	memset(m_previousCollisions, 0, sizeof(m_previousCollisions));

	m_nextCurrentCollisionSlot = 0;
}

void CollisionManager::CheckCollisions()
{
	// Check Player to item box collisions
	PlayerToMonster();
	BulletToPlayer();
	BulletToMonster();

	// Move all current collisions into previous
	memcpy(m_previousCollisions, m_currentCollisions, sizeof(m_currentCollisions));

	// Clear out current collisions
	memset(m_currentCollisions, 0, sizeof(m_currentCollisions));

	// Now current collisions is empty, we'll start adding from the start again
	m_nextCurrentCollisionSlot = 0;

}

bool CollisionManager::ArrayContainsCollision(GameObject* arrayToSearch[], GameObject* first, GameObject* second)
{
	// See if these two GameObjects appear one after the other in specified collisions array
	// Stop one before length so we don't overrun as we'll be checking two elements per iteration
	for (int i = 0; i < MAX_ALLOWED_COLLISIONS - 1; i += 2)
	{
		if ((arrayToSearch[i] == first && arrayToSearch[i + 1] == second) ||
			arrayToSearch[i] == second && arrayToSearch[i + 1] == first)
		{
			// Found them!
			return true;
		}
	}

	// These objects were not colliding last frame
	return false;
}

void CollisionManager::AddCollision(GameObject* first, GameObject* second)
{
	// Add the two colliding objects to the current collisions array
	// We keep track of the next free slot so no searching is required
	m_currentCollisions[m_nextCurrentCollisionSlot] = first;
	m_currentCollisions[m_nextCurrentCollisionSlot + 1] = second;

	m_nextCurrentCollisionSlot += 2;
}

void CollisionManager::PlayerToMonster()
{
	// We'll check each Player against every Monster
	// Note this is not overly efficient, both in readability and runtime performance

	for (unsigned int i = 0; i < m_Players->size(); i++)
	{
		for (unsigned int j = 0; j < m_Monsters->size(); j++)
		{
			// Don't need to store pointer to these objects again but favouring clarity
			// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
			Player* player = (*m_Players)[i];
			Monster* monster = (*m_Monsters)[j];

			CBoundingBox PlayerBounds = player->GetBounds();
			CBoundingBox MonsterBounds = monster->GetBounds();

			// Are they colliding this frame?
			bool isColliding = CheckCollision(PlayerBounds, MonsterBounds);

			// Were they colliding last frame?
			bool wasColliding = ArrayContainsCollision(m_previousCollisions, player, monster);

			if (isColliding)
			{
				// Register the collision
				AddCollision(player, monster);

				if (wasColliding)
				{
					// We are colliding this frame and we were also colliding last frame - that's a collision stay
					// Tell the item box a Player has collided with it (we could pass it the actual Player too if we like)
					monster->OnPlayerCollisionStay(player);
				}
				else
				{
					// We are colliding this frame and we weren't last frame - that's a collision enter
					monster->OnPlayerCollisionEnter(player);
				}
			}
			else
			{
				if (wasColliding)
				{
					// We aren't colliding this frame but we were last frame - that's a collision exit
					monster->OnPlayerCollisionExit(player);
				}
			}
		}
	}
}

void CollisionManager::BulletToPlayer()
{
	// We'll check each Player against every Monster
	// Note this is not overly efficient, both in readability and runtime performance

	for (unsigned int i = 0; i < m_Players->size(); i++)
	{
		for (unsigned int j = 0; j < m_Bullets->size(); j++)
		{
			// Don't need to store pointer to these objects again but favouring clarity
			// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
			Player* Player = (*m_Players)[i];
			Bullet* Bullet = (*m_Bullets)[j];

			CBoundingBox PlayerBounds = Player->GetBounds();
			CBoundingBox BulletBounds = Bullet->GetBounds();

			// Are they colliding this frame?
			bool isColliding = CheckCollision(PlayerBounds, BulletBounds);

			// Were they colliding last frame?
			bool wasColliding = ArrayContainsCollision(m_previousCollisions, Player, Bullet);

			if (isColliding)
			{
				// Register the collision
				AddCollision(Player, Bullet);

				if (wasColliding)
				{
					// We are colliding this frame and we were also colliding last frame - that's a collision stay
					// Tell the item box a Player has collided with it (we could pass it the actual Player too if we like)
					Player->OnBulletCollisionStay(Bullet);
				}
				else
				{
					// We are colliding this frame and we weren't last frame - that's a collision enter
					Player->OnBulletCollisionEnter(Bullet);
				}
			}
			else
			{
				if (wasColliding)
				{
					// We aren't colliding this frame but we were last frame - that's a collision exit
					Player->OnBulletCollisionExit(Bullet);
				}
			}
		}
	}
}

void CollisionManager::BulletToMonster()
{
	// We'll check each Player against every Monster
	// Note this is not overly efficient, both in readability and runtime performance

	for (unsigned int i = 0; i < m_Monsters->size(); i++)
	{
		for (unsigned int j = 0; j < m_Bullets->size(); j++)
		{
			// Don't need to store pointer to these objects again but favouring clarity
			// Can't index into these directly as they're a pointer to a vector. We need to dereference them first
			Monster* Monster = (*m_Monsters)[i];
			Bullet* Bullet = (*m_Bullets)[j];

			CBoundingBox MonsterBounds = Monster->GetBounds();
			CBoundingBox BulletBounds = Bullet->GetBounds();

			// Are they colliding this frame?
			bool isColliding = CheckCollision(MonsterBounds, BulletBounds);

			// Were they colliding last frame?
			bool wasColliding = ArrayContainsCollision(m_previousCollisions, Monster, Bullet);

			if (isColliding)
			{
				// Register the collision
				AddCollision(Monster, Bullet);

				if (wasColliding)
				{
					// We are colliding this frame and we were also colliding last frame - that's a collision stay
					// Tell the item box a Player has collided with it (we could pass it the actual Player too if we like)
					Monster->OnBulletCollisionStay(Bullet);
				}
				else
				{
					// We are colliding this frame and we weren't last frame - that's a collision enter
					Monster->OnBulletCollisionEnter(Bullet);
				}
			}
			else
			{
				if (wasColliding)
				{
					// We aren't colliding this frame but we were last frame - that's a collision exit
					Monster->OnBulletCollisionExit(Bullet);
				}
			}
		}
	}
}