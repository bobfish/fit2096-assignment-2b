#include "Monster.h"

Monster::Monster(Mesh* mesh, Shader* shader, Texture* texture, Vector3 pos, Player* player, MonsterType type)
	: GameObject(mesh, shader, texture, pos)
{
	m_player = player;
	m_type = type;
	m_health = 100;
	m_skill = MathsHelper::RandomRange(3, 10);
	m_isAlive = true;

	m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());
}

Monster::~Monster() {}

int Monster::Attack()
{
	// A monster's attack power is limited to its skill
	return MathsHelper::RandomRange(0, m_skill);
}

void Monster::BeHit(int amount)
{
	// "abs" keeps a value positive
	m_health -= abs(amount);

	if (m_health <= 0)
	{
		m_isAlive = false;
	}
}

void Monster::Update(float timestep)
{
	if (m_isAlive)
	{
		// directionToPlayer = the vector from the Enemy to the player
		// calculated using vector subtraction (see lecture notes for week 2)
		Vector3 directionToPlayer = m_player->GetPosition() - GetPosition();

		// Normalize the vector to get a vector of unit length 
		directionToPlayer.Normalize();

		// Calculate the angle the enemy should be facing
		SetYRotation(atan2(directionToPlayer.x, directionToPlayer.z));

		Vector3 direction = Vector3::TransformNormal(Vector3(0, 0, 1), Matrix::CreateRotationY(GetYRotation()));

		//Movement
		switch (m_type)
		{
		case MonsterType::ONERED: //Chase player constantly.
			m_position += direction * monsterMovementSpeed * speedModifier * timestep;
			break;
		case MonsterType::TWOBLUE: //Run away from player if within 2 tiles
		{
			Vector3 betweenPoints = m_position - m_player->GetPosition();
			if (m_position.x > 1 && m_position.x < BOARD_WIDTH && m_position.z > 1 && m_position.z < BOARD_HEIGHT && betweenPoints.Length() > 5.5)
			{
				m_position += -direction * monsterMovementSpeed * speedModifier * timestep;
			}
		}
		break;
		case MonsterType::THREEGREEN: //Move between randomly selected points.
			if (isReachedDestination)
			{
				int X = MathsHelper::RandomRange(0, BOARD_WIDTH);
				int Z = MathsHelper::RandomRange(0, BOARD_HEIGHT);

				pointToMoveTo = Vector3(X, 0, Z);
				isReachedDestination = false;
			}
			else
			{
				Vector3 betweenPoints = m_position - pointToMoveTo;
				if (betweenPoints.Length() < 0.5)
				{
					isReachedDestination = true;
				}
				else
				{
					//Normalize the direction
					Vector3 destination = pointToMoveTo - GetPosition();
					destination.Normalize();

					// Calculate the angle the enemy should be facing
					SetYRotation(atan2(destination.x, destination.z));

					Vector3 direction = Vector3::TransformNormal(Vector3(0, 0, 1), Matrix::CreateRotationY(GetYRotation()));
					m_position += direction * monsterMovementSpeed * speedModifier * timestep;
				}
			}
			break;
		case MonsterType::FOURYELLOW: //Move towards place 5 tiles in front of player.
		{
			Vector3 betweenPoints = m_position - m_player->GetPosition();
			if (betweenPoints.Length() > 5.5)
			{
				m_position += direction * monsterMovementSpeed * speedModifier * timestep;
			}
		}
		break;
		case MonsterType::FIVEPURPLE: //Stay still until within 3 tiles of player then run away to random spot.
			if (isReachedDestination)
			{
				Vector3 betweenPoints = m_position - m_player->GetPosition();
				if (betweenPoints.Length() < 3)
				{
					int X = MathsHelper::RandomRange(0, BOARD_WIDTH);
					int Z = MathsHelper::RandomRange(0, BOARD_HEIGHT);

					pointToMoveTo = Vector3(X, 0, Z);
					isReachedDestination = false;
				}
			}
			else
			{
				Vector3 betweenPoints = m_position - pointToMoveTo;
				if (betweenPoints.Length() < 0.5)
				{
					isReachedDestination = true;
				}
				else
				{
					//Normalize the direction
					Vector3 destination = pointToMoveTo - GetPosition();
					destination.Normalize();

					// Calculate the angle the enemy should be facing
					SetYRotation(atan2(destination.x, destination.z));

					Vector3 direction = Vector3::TransformNormal(Vector3(0, 0, 1), Matrix::CreateRotationY(GetYRotation()));
					m_position += direction * monsterMovementSpeed * speedModifier * timestep;
				}
			}
			break;
		}

		//Determine if fire or not
		int randPercent = MathsHelper::RandomRange(0, 10000);
		if (randPercent <= fireRate)
		{
			if (m_player->MybulletCount < 50)
			{
				m_player->MygameBullets[m_player->MybulletCount]->Activate(m_position, direction);
				m_player->MybulletCount++;
			}
			else
			{
				m_player->MybulletCount = 0;
			}
		}

		// Keep bounds up to date with position
		m_boundingBox.SetMin(m_position + m_mesh->GetMin());
		m_boundingBox.SetMax(m_position + m_mesh->GetMax());
	}
}

void Monster::OnBulletCollisionEnter(Bullet* other)
{
	if (other->isPlayerMade)
	{
		m_health -= enemyHealthLostWhenHit;
		if (m_health <= 0)
		{
			m_isAlive = false;
		}
	}
}
void Monster::OnBulletCollisionStay(Bullet* other)
{

}
void Monster::OnBulletCollisionExit(Bullet* other)
{

}

void Monster::OnPlayerCollisionEnter(Player* other)
{

}
void Monster::OnPlayerCollisionStay(Player* other)
{

}
void Monster::OnPlayerCollisionExit(Player* other)
{

}