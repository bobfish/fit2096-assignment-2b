/*	FIT2096 - Assignment 1 Sample Solution
*	Monster.h
*	Created by Mike Yeates - 2017 - Monash University
*	A simpe object which has a health, can attack, and can be hit.
*/

#ifndef MONSTER_H
#define MONSTER_H

#include "GameObject.h"
#include "MathsHelper.h"
#include "Player.h"

enum class MonsterType
{
	ONERED,
	TWOBLUE,
	THREEGREEN,
	FOURYELLOW,
	FIVEPURPLE
};

class Monster : public GameObject
{
protected:
	CBoundingBox m_boundingBox;
private:
	int m_health;
	int m_skill;
	bool m_isAlive;

	MonsterType m_type;
	Player* m_player;
	Vector3 pointToMoveTo;
	bool isReachedDestination = true;

public:
	Monster(Mesh* mesh, Shader* shader, Texture* texture, Vector3 pos, Player* player, MonsterType type);
	~Monster();

	int Attack();
	void BeHit(int amount);
	void Update(float timestep);

	bool IsAlive() { return m_isAlive; }
	int GetSkill() { return m_skill; }

	CBoundingBox GetBounds() { return m_boundingBox; }

	void OnBulletCollisionEnter(Bullet* other);
	void OnBulletCollisionStay(Bullet* other);
	void OnBulletCollisionExit(Bullet* other);

	void OnPlayerCollisionEnter(Player* other);
	void OnPlayerCollisionStay(Player* other);
	void OnPlayerCollisionExit(Player* other);
};

#endif