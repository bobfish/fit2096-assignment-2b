#include "Player.h"

Player::Player()
{
	m_input = NULL;
	m_moveSpeed = playerMovementSpeed;
	m_currentBoard = NULL;
	m_movesRemainingBar = NULL;
	m_health = initialHealth;
	m_score = 0;
	m_monstersDefeated = 0;
	m_movesRemaining = MAX_MOVES;

	m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());
}

Player::Player(Mesh* mesh, Shader* shader, Texture* texture, InputController* input, GameBoard* board, GameObject* movesRemainingBar, Camera* camera, Bullet* m_gameBullets[50])
	: GameObject(mesh, shader, texture, Vector3(5,0,5))
{
	m_input = input;
	m_currentBoard = board;
	m_movesRemainingBar = movesRemainingBar;
	m_health = initialHealth;
	m_score = 0;
	m_monstersDefeated = 0;
	m_movesRemaining = MAX_MOVES;
	m_camera = camera;
	MybulletCount = 0;

	m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());
	
	for (int i = 0; i < 50; i++)
	{
		MygameBullets[i] = m_gameBullets[i];
	}
}

Player::~Player() {}

void Player::Update(float timestep)
{
	// Lock moves remaining bar to our position (mesh already takes offset into account)
	m_movesRemainingBar->SetPosition(m_position);
	m_movesRemainingBar->SetXScale((float)m_movesRemaining / MAX_MOVES);

	// We need to identify the frame input was received so we can perform common logic
	// outside of the GetKeyDown IF statements below.
	bool didJustMove = false;

	// We'll start by declaring a world vectors
	Vector3 worldForward = Vector3(0, 0, 1);
	Vector3 worldBackwads = Vector3(0, 0, -1);
	Vector3 worldLeft = Vector3(-1, 0, 0);
	Vector3 worldRight = Vector3(1, 0, 0);

	// Next we'll wrap up our Y rotation in a matrix (remember matrices transform vectors)
	Matrix heading = m_camera->GetHeading();

	// Finally, we'll transform our world forward vector by the heading matrix which 
	// will essentially spin it from a world forward into a local forward which takes
	// the object's rotation into account.
	Vector3 localForward = Vector3::TransformNormal(worldForward, heading);
	Vector3 localBackwards = Vector3::TransformNormal(worldBackwads, heading);
	Vector3 localLeft = Vector3::TransformNormal(worldLeft, heading);
	Vector3 localRight = Vector3::TransformNormal(worldRight, heading);

	//Move up "w" key
	if (m_input->GetKeyHold(0x57))
	{
		Vector3 targetPosition = m_position + localForward * playerMovementSpeed * speedModifier * timestep;

		if (targetPosition.x > 0.5 && targetPosition.x < BOARD_WIDTH - 0.5 && targetPosition.z > 0.5 && targetPosition.x < BOARD_HEIGHT - 0.5)
		{
			m_position += localForward * playerMovementSpeed * speedModifier * timestep;
		}
	}
	//Move down "s" key
	if (m_input->GetKeyHold(0x53))
	{
		Vector3 targetPosition = m_position + localBackwards * playerMovementSpeed * speedModifier * timestep;

		if (targetPosition.x > 0.5 && targetPosition.x < BOARD_WIDTH - 0.5 && targetPosition.z > 0.5 && targetPosition.x < BOARD_HEIGHT - 0.5)
		{
			m_position += localBackwards * playerMovementSpeed * speedModifier * timestep;
		}
	}
	//Move left "a" key
	if (m_input->GetKeyHold(0x41))
	{
		Vector3 targetPosition = m_position + localLeft * playerMovementSpeed * speedModifier * timestep;

		if (targetPosition.x > 0.5 && targetPosition.x < BOARD_WIDTH - 0.5 && targetPosition.z > 0.5 && targetPosition.x < BOARD_HEIGHT - 0.5)
		{
			m_position += localLeft * playerMovementSpeed * speedModifier * timestep;
		}
	}
	//Move right "d" key
	if (m_input->GetKeyHold(0x44))
	{
		Vector3 targetPosition = m_position + localRight * playerMovementSpeed * speedModifier * timestep;

		if (targetPosition.x > 0.5 && targetPosition.x < BOARD_WIDTH - 0.5 && targetPosition.z > 0.5 && targetPosition.x < BOARD_HEIGHT - 0.5)
		{
			m_position += localRight * playerMovementSpeed * speedModifier * timestep;
		}
	}

	//Shoot "space" key
	if (m_input->GetKeyDown(VK_SPACE))
	{
		if (MybulletCount < 50)
		{
			MygameBullets[MybulletCount]->Activate(m_position, localForward);
			MygameBullets[MybulletCount]->isPlayerMade = true;
			MybulletCount++;
		}
		else
		{
			MybulletCount = 0;
		}
	}
	
	//Enable / disable powerup
	if (m_input->GetKeyDown(VK_SHIFT))
	{
		if (speedModifier == 1.0f)
		{
			if (cooldownRemaining > 0)
			{
				speedModifier = 0.25f;
			}
		}
		else
		{
			speedModifier = 1.0f;
		}
	}

	//Reduce cooldownremaining if needed
	if (speedModifier == 0.25f)
	{
		if (cooldownRemaining <= 0)
		{
			speedModifier = 1.0f;
		}
		else
		{
			cooldownRemaining--;
		}
	}

	// Keep bounds up to date with position
	m_boundingBox.SetMin(m_position + m_mesh->GetMin());
	m_boundingBox.SetMax(m_position + m_mesh->GetMax());
}

void Player::OnBulletCollisionEnter(Bullet* other)
{
	if (!other->isPlayerMade)
	{
		m_health -= playerHealthLostWhenHit;
	}
}
void Player::OnBulletCollisionStay(Bullet* other)
{

}
void Player::OnBulletCollisionExit(Bullet* other)
{

}