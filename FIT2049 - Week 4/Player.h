/*	FIT2096 - Assignment 1 Sample Solution
*	Player.h
*	Created by Mike Yeates - 2017 - Monash University
*	A Player listens to the keyboard and responds to arrow key presses.
*	It LERPs itself between cells and also asks the GameBoard where it can move.
*/

#ifndef PLAYER_H
#define PLAYER_H

#include "GameObject.h"
#include "InputController.h"
#include "GameBoard.h"
#include "Bullet.h"
#include "MathsHelper.h"

class Player : public GameObject
{
protected:
	CBoundingBox m_boundingBox;
private:
	// A Player should listen for its own input
	InputController* m_input;

	// We'll be animating our movement so a second position is required
	Vector3 m_targetPosition;
	float m_moveSpeed;
	
	// Which board is the player currently on
	GameBoard* m_currentBoard;

	// The Player will move and scale the moves remaining bar
	GameObject* m_movesRemainingBar;

	//The camera, used for movement
	Camera* m_camera;

	// Game variables
	float m_health;
	int m_score;
	int m_monstersDefeated;
	bool m_isTrapped;
	int m_movesRemaining;
	const static int MAX_MOVES = 100;

public:
	int cooldownRemaining = slowdownPowerAmount;
	Player();
	Player(Mesh* mesh, Shader* shader, Texture* texture, InputController* input, GameBoard* board, GameObject* movesRemainingBar, Camera* camera, Bullet* m_gameBullets[50]);
	~Player();

	void Update(float timestep);

	Bullet* MygameBullets[50];
	int MybulletCount;

	// The Game class will use these to determine if the game should end
	bool GetIsTrapped() { return m_isTrapped; }
	float GetHealth() { return m_health; }
	int GetMovesRemaining() { return m_movesRemaining; }

	CBoundingBox GetBounds() { return m_boundingBox; }

	// Game will use these to output info to the player
	int GetNumberOfMonstersDefeated() { return m_monstersDefeated; }
	int GetScore() { return m_score; }

	void OnBulletCollisionEnter(Bullet* other);
	void OnBulletCollisionStay(Bullet* other);
	void OnBulletCollisionExit(Bullet* other);
};

#endif
