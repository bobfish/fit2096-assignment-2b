/*	FIT2049 - Example Code
*	Camera.cpp
*	Created by Elliott Wilson - 2015 - Monash University
*	Implementation of Camera.h
*/

#include "Camera.h"
#include "MathsHelper.h"

Camera::Camera(InputController* input)
{
	//Set up a "default" camera!
	m_position = Vector3(25.0f, 30.0f, 25.0f);
	m_targetPosition = m_position;
	m_lookAtTarget = Vector3::Zero;
	m_up = Vector3::Up;

	m_aspectRatio = 1280.0f / 720.0f;
	m_fieldOfView = ToRadians(45.0f);
	m_nearClip = 0.1f;
	m_farClip = 100.0f;

	m_viewDirty = true;
	m_projectionDirty = true;

	m_moveSpeed = 1.5f;

	m_input = input;
	m_heading = 0.0f;
	m_pitch = 0.0f;
}

/*Camera::Camera(Vector3 pos, Vector3 lookAt, Vector3 up, float aspect, float fov, float nearClip, float farClip)
{
	m_position = pos;
	m_targetPosition = pos;
	m_lookAtTarget = lookAt;
	m_up = up;

	m_aspectRatio = aspect;
	m_fieldOfView = fov;
	m_nearClip = nearClip;
	m_farClip = farClip;

	m_viewDirty = true;
	m_projectionDirty = true;

	m_moveSpeed = 5.0f;
}*/

Camera::~Camera() {}

void Camera::SetPosition(Vector3 pos)
{
	m_position = pos;
	m_viewDirty = true;		//Every time a value is changed then the respective matrix is set "dirty"
}

void Camera::SetTargetPosition(Vector3 pos)
{
	m_targetPosition = pos;
	m_viewDirty = true;	
}

void Camera::SetLookAt(Vector3 lookAt)
{
	m_lookAtTarget = lookAt;
	m_viewDirty = true;
}

void Camera::SetUp(Vector3 up)
{
	m_up = up;
	m_viewDirty = true;
}

void Camera::SetAspectRatio(float aspect)
{
	m_aspectRatio = aspect;
	m_projectionDirty = true;
}

void Camera::SetFieldOfView(float fov)
{
	m_fieldOfView = fov;
	m_projectionDirty = true;
}

void Camera::SetNearClip(float nearClip)
{
	m_nearClip = nearClip;
	m_projectionDirty = true;
}

void Camera::SetFarClip(float farClip)
{
	m_farClip = farClip;
	m_projectionDirty = true;
}

void Camera::SetHeading(Matrix newHeading)
{
	myHeading = newHeading;
}

void Camera::Update(float timestep)
{
	// Accumulate change in mouse position 
	m_heading += m_input->GetMouseDeltaX() * 0.5f * timestep;
	m_pitch += m_input->GetMouseDeltaY() * 0.5f * timestep;

	// Limit how far the player can look down and up
	m_pitch = MathsHelper::Clamp(m_pitch, ToRadians(-80.0f), ToRadians(80.0f));

	// Wrap heading and pitch up in a matrix so we can transform our look at vector
	// Heading is controlled by MouseX (horizontal movement) but it is a rotation around Y
	// Pitch  controlled by MouseY (vertical movement) but it is a rotation around X
	Matrix heading = Matrix::CreateRotationY(m_heading);
	SetHeading(heading);

	Matrix pitch = Matrix::CreateRotationX(m_pitch);

	// Combine pitch and heading into one matrix for convenience
	Matrix lookAtRotation = pitch * heading;

	// Transform a world forward vector into local space (take pitch and heading into account)
	Vector3 lookAt = Vector3::TransformNormal(Vector3(0, 0, 1), lookAtRotation);

	// At this point, our look-at vector is still relative to the origin
	// Add our position to it so it originates from the camera and points slightly in front of it
	// Remember the look-at vector needs to describe a point in the world relative to the origin
	lookAt += m_position;

	//First person perspective
	SetLookAt(lookAt);
	SetPosition(Vector3::Lerp(m_position, m_targetPosition, m_moveSpeed * timestep));

	if (m_viewDirty)	//We will only recalculate a matrix if it is "dirty"
	{
		m_view = DirectX::XMMatrixLookAtLH(m_position, m_lookAtTarget, m_up);
		m_viewDirty = false;	//Once we recalculate the matrix then it is no longer dirty!
	}

	if (m_projectionDirty)
	{
		m_projection = DirectX::XMMatrixPerspectiveFovLH(m_fieldOfView, m_aspectRatio, m_nearClip, m_farClip);
		m_projectionDirty = false;
	}
}