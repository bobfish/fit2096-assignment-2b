#pragma once

#include "GameObject.h"

class Bullet : public GameObject
{
protected:
	CBoundingBox m_boundingBox;
private:
	Vector3 m_direction;
	bool isRender = true;

public:
	bool isActive = false;
	Bullet();
	Bullet(Mesh* mesh, Shader* shader, Texture* texture);
	void Activate(Vector3 pos, Vector3 direction);
	~Bullet();
	void Update(float timestep);
	bool isPlayerMade = false;

	CBoundingBox GetBounds() { return m_boundingBox; }
};