#pragma once
//Game values, used for balancing game.
static float speedModifier = 1.0f;

//GAME
const static int BOARD_WIDTH = 50;
const static int BOARD_HEIGHT = 50;
const static int numOfHealthCapsules = 30;

const static float bulletSpeed = 20.0f;

//PLAYER
const static float initialHealth = 100.0f;
const static float playerMovementSpeed = 5.0f;
const static int playerHealthLostWhenHit = 25;
const static int slowdownPowerAmount = 1000;


//MONSTERS
const static int monsterMovementSpeed = 3.0f;
const static int numOfMonsters = 5;
const static int fireRate = 10;
const static int enemyHealthLostWhenHit = 25;