#include "Bullet.h"

Bullet::Bullet()
{
	isActive = false;
}

Bullet::Bullet(Mesh* mesh, Shader* shader, Texture* texture)
	: GameObject(mesh, shader, texture, Vector3(-5,0,0))
{
	m_position = Vector3(-5, 0, 0);
	isRender = true;
	isActive = false;
	m_direction = Vector3(0, 0, 0);

	m_boundingBox = CBoundingBox(m_position + m_mesh->GetMin(), m_position + m_mesh->GetMax());

	//Set rotation and scale to look like a bullet
	SetUniformScale(0.15f);
	SetYScale(GetYScale() / 2);
}

Bullet::~Bullet()
{
}

void Bullet::Activate(Vector3 pos, Vector3 direction)
{

	isPlayerMade = false;
	m_position = pos;
	m_direction = direction;

	m_position.y = 0.85;

	isActive = true;
}

void Bullet::Update(float timestep)
{
	if (isActive)
	{
		//check if out of world
		if (m_position.x > BOARD_WIDTH || m_position.z > BOARD_HEIGHT)
		{
			isActive = false;
		}
		else
		{
			//update position
			m_position += m_direction * bulletSpeed * speedModifier * timestep;

			// Keep bounds up to date with position
			m_boundingBox.SetMin(m_position + m_mesh->GetMin());
			m_boundingBox.SetMax(m_position + m_mesh->GetMax());
		}
	}
}