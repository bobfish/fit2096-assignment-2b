#pragma once

#include "GameObject.h"

class HealthCapsule : public GameObject
{
public:
	HealthCapsule(Mesh* mesh, Shader* shader, Texture* texture, Vector3 pos);
	~HealthCapsule();
	void Update(float timeStep);
};